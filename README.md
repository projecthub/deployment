# Deployment

This repo manages the deployments of all our services to our kubernetes cluster.

To apply it, log in to kubectl and run
```bash
kubectl apply -f deployment.yaml
```
in this repo

To port-forward database:
```
kubectl port-forward --namespace default svc/psql-user-management-postgresql 5432:5432
```

Access psql console:
```
export POSTGRES_PASSWORD=$(kubectl get secret --namespace default psql-user-management-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
kubectl port-forward --namespace default svc/psql-user-management-postgresql 5432:5432 &
    PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U postgres -d postgres -p 5432
```