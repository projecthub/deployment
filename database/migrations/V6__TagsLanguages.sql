CREATE TABLE taggings(
  name VARCHAR(255) NOT NULL,
  project_id UUID references projects(id) ON DELETE CASCADE,
  PRIMARY KEY(name, project_id)
);
CREATE TABLE languages(
  code VARCHAR(3) NOT NULL,
  project_id UUID references projects(id) ON DELETE CASCADE,
  PRIMARY KEY(code, project_id)
);