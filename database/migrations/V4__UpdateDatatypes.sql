ALTER TABLE users
    ALTER COLUMN username TYPE VARCHAR(50),
    DROP COLUMN email,
    ALTER COLUMN profile_image TYPE VARCHAR(255),
    ADD COLUMN description VARCHAR(1000) NOT NULL DEFAULT '';
ALTER TABLE projects
    ALTER COLUMN name TYPE VARCHAR(100),
    ALTER COLUMN description TYPE VARCHAR(20000);

CREATE TABLE images (
    id uuid DEFAULT uuid_generate_v4 (),
    url VARCHAR(255) NOT NULL,
    description VARCHAR(150),
    created_at TIMESTAMP DEFAULT now(),
    project uuid NOT NULL REFERENCES projects(id),
    priority real NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);