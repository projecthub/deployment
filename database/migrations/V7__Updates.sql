CREATE TABLE updates(
    id UUID DEFAULT uuid_generate_v4 (),
    project_id UUID NOT NULL REFERENCES projects(id),
    content VARCHAR(1024) NOT NULL,
    images VARCHAR(1024) NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT now(),
    source CHAR(10) NOT NULL DEFAULT 'huddle',
    PRIMARY KEY (id)
);