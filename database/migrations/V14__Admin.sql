CREATE TYPE EventType as ENUM(
    'PROJECT_PUBLISHED',
    'PROJECT_EDITED',
    'USER_REGISTERED'
);
CREATE TYPE Action as ENUM(
    'BAN_PROJECT',
    'BAN_USER',
    'APPROVE'
);
CREATE TABLE events (
    id UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4 (),
    timestamp TIMESTAMP NOT NULL default now(),
    project_id UUID REFERENCES projects(id),
    user_id UUID REFERENCES users(id),
    payload TEXT,
    metadata VARCHAR(2048),
    type EventType NOT NULL,
    available_actions Action [],
    taken_action ACTION
);