ALTER TABLE projects
    ADD COLUMN location VARCHAR(255);

CREATE TABLE projectMessages (
    user_id uuid NOT NULL REFERENCES users(id),
    project_id uuid NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    userIsSender boolean NOT NULL,
    time TIMESTAMP DEFAULT now(),
    content TEXT NOT NULL,
    PRIMARY KEY (user_id, project_id,time)
);
ALTER TABLE users
    ADD COLUMN name VARCHAR(255);