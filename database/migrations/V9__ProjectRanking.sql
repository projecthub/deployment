ALTER TABLE projects
ADD COLUMN IF NOT EXISTS public BOOL NOT NULL DEFAULT false,
    ADD COLUMN IF NOT EXISTS last_description_update TIMESTAMP NOT NULL DEFAULT now();
ALTER TABLE project_saves DROP CONSTRAINT project_saves_project_id_fkey,
    ADD CONSTRAINT project_saves_project_id_fkey FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE;
ALTER TABLE project_saves DROP CONSTRAINT project_saves_user_id_fkey,
    ADD CONSTRAINT project_saves_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE updates DROP CONSTRAINT updates_project_id_fkey,
    ADD CONSTRAINT updates_project_id_fkey FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE;
ALTER TABLE projectMessages DROP CONSTRAINT projectMessages_user_id_fkey,
    ADD CONSTRAINT projectMessages_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE images DROP CONSTRAINT images_project_fkey,
    ADD CONSTRAINT images_project_fkey FOREIGN KEY(project) REFERENCES projects(id) ON DELETE CASCADE;
ALTER TABLE messages DROP CONSTRAINT messages_sender_id_fkey,
    ADD CONSTRAINT messages_sender_id_fkey FOREIGN KEY(sender_id) REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE messages DROP CONSTRAINT messages_receiver_id_fkey,
    ADD CONSTRAINT messages_receiver_id_fkey FOREIGN KEY(receiver_id) REFERENCES users(id) ON DELETE CASCADE;
CREATE VIEW projects_ranked AS
SELECT projects.*,
    update_timestamps.last_update_timestamp,
    GREATEST(last_description_update, last_update_timestamp) last_change,
    (COALESCE(
        4.0 * exp(
            -0.1 * extract(
                epoch
                from AGE(
                        GREATEST(last_description_update, last_update_timestamp)
                    )
            ) /(60.0 * 60 * 24)
        ),
        0.0
    ) + sqrt(COALESCE(save_counts.save_count::float, 0.0)))::float AS score
FROM projects
    LEFT JOIN (
        SELECT COUNT(*) AS save_count,
            project_id
        FROM project_saves
        GROUP BY project_id
    ) save_counts ON projects.id = save_counts.project_id
    LEFT JOIN(
        SELECT max(timestamp) AS last_update_timestamp,
            project_id
        FROM updates
        GROUP BY project_id
    ) update_timestamps ON update_timestamps.project_id = save_counts.project_id
WHERE projects.public = true;