CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS users(
    id uuid PRIMARY KEY,
    username TEXT NOT NULL,
    email TEXT NOT NULL,
    profile_image TEXT
);
CREATE TABLE projects (
    id uuid DEFAULT uuid_generate_v4 (),
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT now(),
    creator uuid NOT NULL REFERENCES users(id),
    PRIMARY KEY (id)
);
