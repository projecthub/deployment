DROP VIEW projects_ranked;
CREATE VIEW projects_ranked AS
SELECT projects.*,
    update_timestamps.last_update_timestamp,
    GREATEST(last_description_update, last_update_timestamp) last_change,
    (
        COALESCE(
            4.0 * exp(
                -0.1 * extract(
                    epoch
                    from AGE(
                            GREATEST(last_description_update, last_update_timestamp)
                        )
                ) /(60.0 * 60 * 24)
            ),
            0.0
        ) + sqrt(COALESCE(save_counts.save_count::float, 0.0))
    )::float AS score
FROM projects
    LEFT JOIN (
        SELECT COUNT(*) AS save_count,
            project_id
        FROM project_saves
        GROUP BY project_id
    ) save_counts ON projects.id = save_counts.project_id
    LEFT JOIN(
        SELECT max(timestamp) AS last_update_timestamp,
            project_id
        FROM updates
        GROUP BY project_id
    ) update_timestamps ON update_timestamps.project_id = save_counts.project_id
WHERE projects.public = true;