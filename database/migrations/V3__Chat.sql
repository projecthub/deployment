CREATE TABLE messages (
    sender_id uuid NOT NULL REFERENCES users(id),
    receiver_id uuid NOT NULL REFERENCES users(id),
    time TIMESTAMP DEFAULT now(),
    content TEXT NOT NULL,
    PRIMARY KEY (sender_id, receiver_id,time)
);