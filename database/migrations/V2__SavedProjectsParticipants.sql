CREATE TABLE IF NOT EXISTS project_saves(
    user_id uuid NOT NULL REFERENCES users(id),
    project_id uuid NOT NULL REFERENCES projects(id),
    PRIMARY KEY (user_id, project_id),
    created_at TIMESTAMP DEFAULT now()
);
CREATE TABLE participations (
    user_id uuid NOT NULL REFERENCES users(id),
    project_id uuid NOT NULL REFERENCES projects(id),
    PRIMARY KEY (user_id, project_id),
    created_at TIMESTAMP DEFAULT now()
);