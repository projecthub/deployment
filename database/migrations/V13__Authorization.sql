CREATE TYPE authorization_level as ENUM(
    'none',
    'email_verified',
    'tum_verified',
    'trusted',
    'admin'
);
ALTER TABLE users
ADD COLUMN authorization_level authorization_level NOT NULL default 'none';