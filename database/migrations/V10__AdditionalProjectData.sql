CREATE TYPE project_status as ENUM (
    'idea',
    'early',
    'established',
    'suspended',
    'closed'
);
ALTER TABLE projects
ADD COLUMN status project_status NOT NULL default 'idea',
    ADD COLUMN member_count INTEGER NOT NULL default 0;
