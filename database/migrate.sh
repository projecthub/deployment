export POSTGRES_PASSWORD=$(kubectl get secret --namespace default psql-postgresql -o jsonpath="{.data.postgres-password}" | base64 --decode)
kubectl port-forward --namespace default svc/psql-postgresql 5432:5432 &
sleep 1
flyway -url="jdbc:postgresql://localhost:5432/postgres" -user=postgres  -password=$POSTGRES_PASSWORD -locations="filesystem:$(git rev-parse --show-toplevel)/database/migrations" migrate
kill $(jobs -p)